#!/usr/bin/env python
# -*- coding: utf-8 -*-

version = "0.0.1-draft"

import sys
import datetime
import psutil
#import Gnuplot

# if int(sys.version[0]) < 3:
#     from ConfigParser import SafeConfigParser
# else:
#     from configparser import SafeConfigParser

import config

sys.path.append("lib")

args = len(sys.argv)
prog = sys.argv[0]
if (args > 1):
    arg1 = sys.argv[1]

prog_name = "smon"
prog_desc = "Simple Monitor"

help_opt = [ 'h', '-h', '-H', '--help', '-help', 'help', '?' ]
conf_opt = [ 'i', '-i' ]
vers_opt = [ 'v', '-v', '-V', '--version', '-version', 'version' ]
stat_opt = [ 's' '-s' ]
coll_opt = [ 'c' '-c' ]
stop_opt = [ 'x' '-x' ]
print_opt = [ 'g' '-g' ]


def print_help():
    "Print out usage and help."
    print("""
    {0} {1}

    Usage: {2} <option>

    <option> in one of the following:
    {3}  Print configuration
    {4}  Show this help
    {5}  Print version
    """.format(prog_desc, version, prog, conf_opt[1], help_opt[1], vers_opt[1]))

def print_conf():
    "Print out saved configuration."
    print

def ck_args():
    "Check arguments correctness and related actions."
    if (arg1 in help_opt):
        print_help()
        sys.exit(0)
    elif (arg1 in conf_opt):
        print_conf()
        sys.exit(0)
    elif (arg1 in vers_opt):
        print("{0} {1}".format(prog_name, version))
        sys.exit(0)
    elif (arg1 in stat_opt):

        # show data gathering status
        print
    elif (arg1 in coll_opt):
        # start gathering data
        print
    elif (arg1 in stop_opt):
        # stop data gathering
        print
    elif (arg1 in print_opt):
        # print some graph?
        print
    else:
        print_help()
        sys.exit(1)

def doact():
    print

def main():
    "Start the program."
    ck_args()
    # do action (status, start, stop, print)


if __name__ == "__main__":
    main()
