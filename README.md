
# smon
*simple system monitor and resources usage collector*

Please read LICENSE.txt file.


### Intro
We all love stats and graphs!


### What it does
This simple program collects various system data and produces some nice graphs: img/svg? ascii?

We'll see!

In addiction to standard resources (CPU, MEM, DISKIO, etc..), that can be even disabled, extra monitors can be easily created.


### Usage
You'll need the following:

* psutil (python lib)
* GnuPlot (program)

From your favorite terminal emulator, issue:

`$ python smon.py`


### Versions

All tests performed with following software versions:

* python            2.7.10
* python            3.4.3
* python-psutils    2.2.1
* GnuPlot           4.6-p6


### FAQ

**Why plain GnuPlot?**

* *matplotlib* has no ASCII output and *gnuplot lib* has no python3 support. Hints are welcome!

**Why don't you use {YAML | INI | JSON | XML} for config?**

* a different format (YAML more likely) will be considered as program will gain needs.
